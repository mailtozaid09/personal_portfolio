import React from 'react'
import { useNavigate } from 'react-router-dom'

import { FaUser } from 'react-icons/fa';
import { IoMdDocument } from 'react-icons/io';
import { MdComputer, MdPermContactCalendar } from 'react-icons/md';
import Button from '../button';


const nav_options = [
    {
        title: 'profile',
        icon: <FaUser style = {{ color: "#ffffff", }}  size={30} />
    },
    {
        title: 'about',
        icon: <FaUser style = {{ color: "#ffffff", }}  size={30} />
    },
    {
        title: 'work',
        icon: <MdComputer style = {{ color: "#ffffff", }}  size={30} />
    },
    {
        title: 'contact',
        icon: <MdPermContactCalendar style = {{ color: "#ffffff", }}  size={30} />
    },
    {
        title: 'resume',
        icon: <IoMdDocument style = {{ color: "#ffffff", }}  size={30} />
    },
]
const Navbar = () => {

    const navigate = useNavigate()

    const navFunction = (item) => {

        var path = item.title

        if(path == 'profile'){
            navigate('/')
        }else if(path == 'about') {
            navigate('/about')
        }else if(path == 'work') {
            navigate('/work')
        }else if(path == 'contact') {
            navigate('/contact')
        }else if(path == 'resume'){
        
        }else{
            navigate('/')
        }



    }

    return (
        <div className='navbar_main_container' >
            <div className='navbar_container' >
                {nav_options?.map((item, index) => (
                    <div onClick={() => {navFunction(item)}} className='nav_options_container' >
                        {item.title == 'profile'
                        ?
                        <div style={{height: 30, width: 30, borderWidth: 1, borderRadius: 15, borderColor: 'red'}} ></div>
                        :
                        <div >{item.icon}</div>
                        }
                    </div>
                ))}
                <div className='nav_hide_button'>
                    <Button
                        title="Say Hello"
                        icon="👋"
                    />
                </div>
            </div>
        </div>
    )
}

export default Navbar