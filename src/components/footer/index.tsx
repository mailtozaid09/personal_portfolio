import React from 'react'
import './footer.css';

import DeveloperImg from '../custom/DeveloperImg'
import Button from '../button';


const nav_options = [
    {
        title: 'profile',
    },
    {
        title: 'about-me',
    },
    {
        title: 'work',
    },
    {
        title: 'contact',
    },
]

const Footer = () => {
    return (
        <div className='card_container mb-32' >
            <div className='footer_container' >
                
                <div className='xl:mr-16' >
                    <DeveloperImg />
                </div>

                <div className='footer_content_container' >
                    <h1 className='footer_title' >Ready to kickstart your project with a touch of magic? </h1>
                    <div className='footer_title_subtitle text-gray-50 mb-6' >
                        <span className='text-white' >Reach out and let's make it happen ✨. </span>
                        <span className='text-dark-5 pl-1'> I'm also available for full-time or Part-time opportunities to push the boundaries of design and deliver exceptional work.</span>
                    </div>
                    <Button
                        title="Let' Talk"
                        icon="🤩"
                    />
                </div>
               
            </div>
        </div>
    )
}

export default Footer