import React from 'react'
import './button.css';

import { useNavigate } from 'react-router-dom'

import { FaUser } from 'react-icons/fa';
import { IoMdDocument } from 'react-icons/io';
import { MdComputer, MdPermContactCalendar } from 'react-icons/md';

const Button = ({title, icon, onPress}) => {

    const navigate = useNavigate()

    return (
        <div onClick={onPress} className='button_main_container' >
            <h1>{title}</h1>
            {icon && (
                <div className='text-2xl' >
                    {icon}
                </div>
            )}
        </div>
    )
}

export default Button