import React from 'react'
import './home.css'
import Button from '../../components/button'
import DeveloperImg from '../../components/custom/DeveloperImg'
import Work from '../work'
import Experience from '../experience'


const Home = () => {
    return (
        < >
            <div className='home_container' >
              

                <div className='home_content_container' >
                    <h3 className='text-white' >Hey there!</h3>
                    <h1 className='home_title py-2 pb-6' >I'm Paresh, a product designer crafting user-centric design with pixel-perfect precision.</h1>
                    <h3 className=' text-white pb-4' >Hey there!</h3>

                    <div className='lets_talk_button' >
                        <Button
                            title="Let's Talk"
                            icon="👋"
                        />
                    </div>
                </div>
               
                <div className='xl:ml-16' >
                    <DeveloperImg />
                </div>

            </div>



            <Experience />


            <Work />
        </>
    )
}

export default Home