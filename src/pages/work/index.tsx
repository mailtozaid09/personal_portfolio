import React from 'react'
import './work.css'

import Button from '../../components/button'
import DeveloperImg from '../../components/custom/DeveloperImg'

const Work = () => {
    return (
        <div className='work_container' >
            <div>
                <h3 className='text-white' >Works & Projects</h3>
                <h1 className='work_title py-2 pb-6'>Check out some of my design projects, meticulously crafted with love and dedication, each one reflecting the passion and soul I poured into every detail.</h1>
                
                <div class="work_projects_container">
                    <div className='work_project_item' >01</div>
            
                    <div className='work_project_item' >09</div>
                    <div className='work_project_item' >01</div>
            
                    <div className='work_project_item' >09</div>
                    <div className='work_project_item' >01</div>
                    
                    <div className='work_project_item' >09</div>

                </div>
            </div>
        </div>
    )
}

export default Work