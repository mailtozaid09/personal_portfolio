import React from 'react'
import './experience.css'
import Button from '../../components/button'
import DeveloperImg from '../../components/custom/DeveloperImg'
import Work from '../work'

const experience = [
    {
        title: 'Mobile App Developer',
        company: 'Klipit',
        duration: 'Nov, 2023-Present'
    },
    {
        title: 'Salesforce Developer',
        company: 'Koch Buisness Solutions',
        duration: 'Jan-Nov, 2023'
    },
    {
        title: 'Frontend Developer Intern',
        company: 'Voosh Food Private Ltd.',
        duration: 'Jun-Nov, 2022'
    },
    {
        title: 'Mobile App Developer Intern',
        company: 'Feature Ventures',
        duration: 'Apr-Jun, 2022'
    },
]

const education = [
    {
        title: 'Christ University, Bangalore',
        company: 'Btech (IT)',
        duration: '2019-2023'
    },
    {
        title: 'The Hritage School, Dehradun',
        company: 'Class 12',
        duration: '2017-2018'
    },
    {
        title: 'The Hritage School, Dehradun',
        company: 'Class 10',
        duration: '2015-2016'
    },
]


const skills = [
    'React Native',
    'React',
    'Redux',
    'Firebase',
    'MongoDB',
    'Express',
    'Node JS',
    'Javascript',
    'Tailwind',
]


const stacks = [
    {
        title: 'Christ University, Bangalore',
        company: 'Btech (IT)',
        duration: '2019-2023'
    },
    {
        title: 'The Hritage School, Dehradun',
        company: 'Class 12',
        duration: '2017-2018'
    },
    {
        title: 'The Hritage School, Dehradun',
        company: 'Class 10',
        duration: '2015-2016'
    },
]



const Experience = () => {
    return (
        < >
            <div className='exp_container' >
            
                <div className='exp_card_container'>
                    <div className='exp_card' >
                        <div className='exp_card_title' >Experience <span className='text-base' >02 Years</span></div>

                        <div>
                            {experience?.map((item) => 
                                <div className='w-full' >
                                    <div className='w-full mt-4' >
                                        <div className='flex flex-row items-center justify-between mb-1' >
                                            <div className='text-base' >{item?.company}</div>
                                            <div className='text-xs text-light-2'>{item?.duration}</div>
                                        </div>
                                        <div className='text-sm text-light'>{item?.title}</div>
                                    </div>
                                </div>
                            )}
                        </div>
                    </div>

                    <div className='exp_card_container_center skils_hide_button' >
                        <div className='exp_card' >
                            <div className='exp_card_title' >Skills & Expertise</div>
                            <div className='flex flex-wrap mt-4' >
                                {skills?.map((item, index) => (
                                    <div className='skils_container' key={index} >
                                        <div className='text-xs text-light-2' >{item}</div>
                                    </div>
                                ))}
                            </div>
                        </div>
                        <div className='exp_card mt-4 ' >
                            <div className='exp_card_title' >Essential Stacks</div>
                            <div className='text-xs text-light  mt-4' >A Comprehensive Collection of Useful Tools to Support and Optimize My Workflow</div>
                            <div className='flex flex-wrap mt-4' >
                                
                                
                                {/* <!-- Stack overflow --> */}
                                 <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="h-7 w-7"
                                    fill="currentColor"
                                    style={{ color: "#f48024", marginRight: 14 }}
                                    viewBox="0 0 24 24"
                                >
                                    <path d="M15 21h-10v-2h10v2zm6-11.665l-1.621-9.335-1.993.346 1.62 9.335 1.994-.346zm-5.964 6.937l-9.746-.975-.186 2.016 9.755.879.177-1.92zm.538-2.587l-9.276-2.608-.526 1.954 9.306 2.5.496-1.846zm1.204-2.413l-8.297-4.864-1.029 1.743 8.298 4.865 1.028-1.744zm1.866-1.467l-5.339-7.829-1.672 1.14 5.339 7.829 1.672-1.14zm-2.644 4.195v8h-12v-8h-2v10h16v-10h-2z" />
                                </svg>


                                {/* Github */}
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="h-7 w-7"
                                    fill="currentColor"
                                    style={{ color: "#fff", marginRight: 14 }}
                                    viewBox="0 0 24 24"
                                >
                                    <path d="M12 0c-6.626 0-12 5.373-12 12 0 5.302 3.438 9.8 8.207 11.387.599.111.793-.261.793-.577v-2.234c-3.338.726-4.033-1.416-4.033-1.416-.546-1.387-1.333-1.756-1.333-1.756-1.089-.745.083-.729.083-.729 1.205.084 1.839 1.237 1.839 1.237 1.07 1.834 2.807 1.304 3.492.997.107-.775.418-1.305.762-1.604-2.665-.305-5.467-1.334-5.467-5.931 0-1.311.469-2.381 1.236-3.221-.124-.303-.535-1.524.117-3.176 0 0 1.008-.322 3.301 1.23.957-.266 1.983-.399 3.003-.404 1.02.005 2.047.138 3.006.404 2.291-1.552 3.297-1.23 3.297-1.23.653 1.653.242 2.874.118 3.176.77.84 1.235 1.911 1.235 3.221 0 4.609-2.807 5.624-5.479 5.921.43.372.823 1.102.823 2.222v3.293c0 .319.192.694.801.576 4.765-1.589 8.199-6.086 8.199-11.386 0-6.627-5.373-12-12-12z" />
                                </svg>

                                {/* <!-- Linkedin --> */}
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="h-7 w-7"
                                    fill="currentColor"
                                    style={{ color: "#0077b5", marginRight: 14 }}
                                    viewBox="0 0 24 24"
                                >
                                    <path d="M4.98 3.5c0 1.381-1.11 2.5-2.48 2.5s-2.48-1.119-2.48-2.5c0-1.38 1.11-2.5 2.48-2.5s2.48 1.12 2.48 2.5zm.02 4.5h-5v16h5v-16zm7.982 0h-4.968v16h4.969v-8.399c0-4.67 6.029-5.052 6.029 0v8.399h4.988v-10.131c0-7.88-8.922-7.593-11.018-3.714v-2.155z" />
                                </svg>

                                {/* <!-- Twitter --> */}
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="h-7 w-7"
                                    fill="currentColor"
                                    style={{ color: "#1da1f2", marginRight: 14 }}
                                    viewBox="0 0 24 24"
                                >
                                    <path d="M24 4.557c-.883.392-1.832.656-2.828.775 1.017-.609 1.798-1.574 2.165-2.724-.951.564-2.005.974-3.127 1.195-.897-.957-2.178-1.555-3.594-1.555-3.179 0-5.515 2.966-4.797 6.045-4.091-.205-7.719-2.165-10.148-5.144-1.29 2.213-.669 5.108 1.523 6.574-.806-.026-1.566-.247-2.229-.616-.054 2.281 1.581 4.415 3.949 4.89-.693.188-1.452.232-2.224.084.626 1.956 2.444 3.379 4.6 3.419-2.07 1.623-4.678 2.348-7.29 2.04 2.179 1.397 4.768 2.212 7.548 2.212 9.142 0 14.307-7.721 13.995-14.646.962-.695 1.797-1.562 2.457-2.549z" />
                                </svg>

                               
                                {/* Behance */}
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="h-7 w-7"
                                    fill="currentColor"
                                    style={{ color: "#1769ff", marginRight: 14 }}
                                    viewBox="0 0 24 24"
                                >
                                    <path d="M22 7h-7v-2h7v2zm1.726 10c-.442 1.297-2.029 3-5.101 3-3.074 0-5.564-1.729-5.564-5.675 0-3.91 2.325-5.92 5.466-5.92 3.082 0 4.964 1.782 5.375 4.426.078.506.109 1.188.095 2.14h-8.027c.13 3.211 3.483 3.312 4.588 2.029h3.168zm-7.686-4h4.965c-.105-1.547-1.136-2.219-2.477-2.219-1.466 0-2.277.768-2.488 2.219zm-9.574 6.988h-6.466v-14.967h6.953c5.476.081 5.58 5.444 2.72 6.906 3.461 1.26 3.577 8.061-3.207 8.061zm-3.466-8.988h3.584c2.508 0 2.906-3-.312-3h-3.272v3zm3.391 3h-3.391v3.016h3.341c3.055 0 2.868-3.016.05-3.016z" />
                                </svg>



                            </div>
                        </div>
                    </div>

                    <div className='exp_card' >
                        <div className='exp_card_title' >Education</div>

                        <div>
                            {education?.map((item) => 
                                <div className='w-full' >
                                    <div className='w-full mt-4' >
                                        <div className='flex flex-row items-center justify-between mb-1' >
                                            <div className='text-base' >{item?.company}</div>
                                            <div className='text-xs text-light-2'>{item?.duration}</div>
                                        </div>
                                        <div className='text-sm text-light'>{item?.title}</div>
                                    </div>
                                </div>
                            )}
                        </div>
                    </div>
                </div>
               

                <div className='exp_card_container_center show_button mt-4' >
                    <div className='exp_card' >
                        <div className='exp_card_title' >Skills & Expertise</div>
                        <div className='flex flex-wrap mt-4' >
                            {skills?.map((item, index) => (
                                <div className='skils_container' key={index} >
                                    <div className='text-xs text-light-2' >{item}</div>
                                </div>
                            ))}
                        </div>
                    </div>
                    <div className='exp_card mt-4 ' >
                            <div className='exp_card_title' >Essential Stacks</div>
                            <div className='text-xs text-light  mt-4' >A Comprehensive Collection of Useful Tools to Support and Optimize My Workflow</div>
                            <div className='flex flex-wrap mt-4' >
                                
                                
                                {/* <!-- Stack overflow --> */}
                                 <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="h-7 w-7"
                                    fill="currentColor"
                                    style={{ color: "#f48024", marginRight: 14 }}
                                    viewBox="0 0 24 24"
                                >
                                    <path d="M15 21h-10v-2h10v2zm6-11.665l-1.621-9.335-1.993.346 1.62 9.335 1.994-.346zm-5.964 6.937l-9.746-.975-.186 2.016 9.755.879.177-1.92zm.538-2.587l-9.276-2.608-.526 1.954 9.306 2.5.496-1.846zm1.204-2.413l-8.297-4.864-1.029 1.743 8.298 4.865 1.028-1.744zm1.866-1.467l-5.339-7.829-1.672 1.14 5.339 7.829 1.672-1.14zm-2.644 4.195v8h-12v-8h-2v10h16v-10h-2z" />
                                </svg>


                                {/* Github */}
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="h-7 w-7"
                                    fill="currentColor"
                                    style={{ color: "#fff", marginRight: 14 }}
                                    viewBox="0 0 24 24"
                                >
                                    <path d="M12 0c-6.626 0-12 5.373-12 12 0 5.302 3.438 9.8 8.207 11.387.599.111.793-.261.793-.577v-2.234c-3.338.726-4.033-1.416-4.033-1.416-.546-1.387-1.333-1.756-1.333-1.756-1.089-.745.083-.729.083-.729 1.205.084 1.839 1.237 1.839 1.237 1.07 1.834 2.807 1.304 3.492.997.107-.775.418-1.305.762-1.604-2.665-.305-5.467-1.334-5.467-5.931 0-1.311.469-2.381 1.236-3.221-.124-.303-.535-1.524.117-3.176 0 0 1.008-.322 3.301 1.23.957-.266 1.983-.399 3.003-.404 1.02.005 2.047.138 3.006.404 2.291-1.552 3.297-1.23 3.297-1.23.653 1.653.242 2.874.118 3.176.77.84 1.235 1.911 1.235 3.221 0 4.609-2.807 5.624-5.479 5.921.43.372.823 1.102.823 2.222v3.293c0 .319.192.694.801.576 4.765-1.589 8.199-6.086 8.199-11.386 0-6.627-5.373-12-12-12z" />
                                </svg>

                                {/* <!-- Linkedin --> */}
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="h-7 w-7"
                                    fill="currentColor"
                                    style={{ color: "#0077b5", marginRight: 14 }}
                                    viewBox="0 0 24 24"
                                >
                                    <path d="M4.98 3.5c0 1.381-1.11 2.5-2.48 2.5s-2.48-1.119-2.48-2.5c0-1.38 1.11-2.5 2.48-2.5s2.48 1.12 2.48 2.5zm.02 4.5h-5v16h5v-16zm7.982 0h-4.968v16h4.969v-8.399c0-4.67 6.029-5.052 6.029 0v8.399h4.988v-10.131c0-7.88-8.922-7.593-11.018-3.714v-2.155z" />
                                </svg>

                                {/* <!-- Twitter --> */}
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="h-7 w-7"
                                    fill="currentColor"
                                    style={{ color: "#1da1f2", marginRight: 14 }}
                                    viewBox="0 0 24 24"
                                >
                                    <path d="M24 4.557c-.883.392-1.832.656-2.828.775 1.017-.609 1.798-1.574 2.165-2.724-.951.564-2.005.974-3.127 1.195-.897-.957-2.178-1.555-3.594-1.555-3.179 0-5.515 2.966-4.797 6.045-4.091-.205-7.719-2.165-10.148-5.144-1.29 2.213-.669 5.108 1.523 6.574-.806-.026-1.566-.247-2.229-.616-.054 2.281 1.581 4.415 3.949 4.89-.693.188-1.452.232-2.224.084.626 1.956 2.444 3.379 4.6 3.419-2.07 1.623-4.678 2.348-7.29 2.04 2.179 1.397 4.768 2.212 7.548 2.212 9.142 0 14.307-7.721 13.995-14.646.962-.695 1.797-1.562 2.457-2.549z" />
                                </svg>

                               
                                {/* Behance */}
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="h-7 w-7"
                                    fill="currentColor"
                                    style={{ color: "#1769ff", marginRight: 14 }}
                                    viewBox="0 0 24 24"
                                >
                                    <path d="M22 7h-7v-2h7v2zm1.726 10c-.442 1.297-2.029 3-5.101 3-3.074 0-5.564-1.729-5.564-5.675 0-3.91 2.325-5.92 5.466-5.92 3.082 0 4.964 1.782 5.375 4.426.078.506.109 1.188.095 2.14h-8.027c.13 3.211 3.483 3.312 4.588 2.029h3.168zm-7.686-4h4.965c-.105-1.547-1.136-2.219-2.477-2.219-1.466 0-2.277.768-2.488 2.219zm-9.574 6.988h-6.466v-14.967h6.953c5.476.081 5.58 5.444 2.72 6.906 3.461 1.26 3.577 8.061-3.207 8.061zm-3.466-8.988h3.584c2.508 0 2.906-3-.312-3h-3.272v3zm3.391 3h-3.391v3.016h3.341c3.055 0 2.868-3.016.05-3.016z" />
                                </svg>



                            </div>
                        </div>
                </div>
            </div>
        </>
    )
}

export default Experience