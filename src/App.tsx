import React from 'react'
import './globals.css'
import { Route, Routes } from 'react-router-dom'


import Home from './pages/home'
import About from './pages/about'
import Navbar from './components/navbar'
import Footer from './components/footer'
import Work from './pages/work'
import Contact from './pages/contact'

const App = () => {
    return (
        <main>
            <Navbar />
            {/* <Routes>
                <Route path='/' element={<Home />} />
                <Route path='/about' element={<About />} />
                <Route path='/work' element={<Work />} />
                <Route path='/contact' element={<Contact />} />
            </Routes> */}

            {/*
           
            <About /> */}
              <Home />
            <Footer />
        </main>
    )
}

export default App